<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\DateDto;
use App\Form\DateType;
use App\Form\FormHandler;
use App\Model\Date;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    private FormHandler $formHandler;

    public function __construct(FormHandler $formHandler)
    {
        $this->formHandler = $formHandler;
    }

    /**
     * @Route("/", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $handledData = $this->formHandler->handleFromRequest($request, DateType::class, new Date());
        if ($handledData instanceof Date) {
            return $this->render('result.html.twig', [
                'data' => DateDto::createFromDateModel($handledData),
            ]);
        }

        return $this->render('index.html.twig', [
            'form' => $handledData->createView(),
        ]);
    }
}
