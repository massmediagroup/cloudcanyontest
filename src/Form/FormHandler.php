<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class FormHandler
{
    private FormFactoryInterface $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function handleFromRequest(Request $request, string $type, $entity, array $options = [])
    {
        $form = $this->formFactory->create($type, $entity, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $form->getData();
        }

        return $form;
    }
}
