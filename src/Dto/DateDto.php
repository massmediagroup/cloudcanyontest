<?php

declare(strict_types=1);

namespace App\Dto;

use App\Model\Date;
use DateTime;
use DateTimeInterface;
use DateTimeZone;

class DateDto
{
    public DateTimeInterface $date;

    public function __construct(string $date, string $timezone)
    {
        $this->date = new DateTime($date, new DateTimeZone($timezone));
    }

    public static function createFromDateModel(Date $date): self
    {
        return new self($date->getDate(), $date->getTimezone());
    }

    public function isLeap(): bool
    {
        return $this->date->format('L') === '1';
    }

    public function getDaysNumber(): int
    {
        return (int)$this->date->format('t');
    }

    public function getMonthName(): string
    {
        return $this->date->format('F');
    }

    public function getTimezone(): string
    {
        return $this->date->getTimezone()->getName();
    }

    public function getUtcOffset(): int
    {
        $offset = $this->date->getTimezone()->getOffset(
            new DateTime($this->date->format(DateTime::ATOM), new DateTimeZone('UTC'))
        );

        return $offset / 60;
    }
}
